package com.citi.training.employees.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.employees.exceptions.EmployeeNotFoundException;
import com.citi.training.employees.model.Employee;

@Component
public class MysqlEmployeeDao implements EmployeeDao {

	@Autowired
	JdbcTemplate tpl;

	public List<Employee> findAll() {
		return tpl.query("SELECT id, name, salary FROM employee", new EmployeeMapper());

	}
	
	public Employee findById(int id) {
		
		List<Employee> employees = tpl.query("SELECT * FROM employee WHERE ID = ?", 
				new Object [] {id}, new EmployeeMapper());
		
		if(employees.size() <= 0) {
			throw new EmployeeNotFoundException("Employee with id =[" + id + "] not found");
		}
		
		return employees.get(0);
		
	}

	@Override
    public Employee create(Employee employee) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        this.tpl.update(
            new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps =
                            connection.prepareStatement("insert into employee (name, salary) values (?, ?)",
                            Statement.RETURN_GENERATED_KEYS);
                    ps.setString(1, employee.getName());
                    ps.setDouble(2, employee.getSalary());
                    return ps;
                }
            },
            keyHolder);
        employee.setId(keyHolder.getKey().intValue());
        return employee;
    }


    public void deleteById(int id) {
    	tpl.update("DELETE FROM employee WHERE ID = ?", id);
    	
    	
    }

	private static final class EmployeeMapper implements RowMapper<Employee> {
		
		public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Employee(rs.getInt("id"), 
								rs.getString("name"), 
								rs.getDouble("salary"));
			
		}
	}
}
