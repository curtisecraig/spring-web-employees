package com.citi.training.employees.dao;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.employees.exceptions.EmployeeNotFoundException;
import com.citi.training.employees.model.Employee;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class MysqlEmployeeDaoTests {
	
	@Autowired
	MysqlEmployeeDao mysqlEmployeeDao;
	
	@Test
	@Transactional
	public void test_createAndFindAll() {
		mysqlEmployeeDao.create(new Employee(-1, "Curtis", 100000));
		
		assertEquals(mysqlEmployeeDao.findAll().size(), 1);
	}
	
	@Test
	@Transactional
	public void test_findById() {
		Employee createdEmp = mysqlEmployeeDao.create(new Employee(-1, "Curtis", 100000));
		Employee newEmp = mysqlEmployeeDao.findById(createdEmp.getId());
		
		assertEquals(newEmp.getName(), "Curtis");
		assertEquals(newEmp.getSalary(), 100000, 0.0001);
	}
	
	@Test(expected=EmployeeNotFoundException.class)
	@Transactional
	public void test_findById_throwsNotFound() {
		Employee createdEmp = mysqlEmployeeDao.create(new Employee(-1, "Curtis", 100000));
		Employee newEmp = mysqlEmployeeDao.findById(999999);
	}

}
